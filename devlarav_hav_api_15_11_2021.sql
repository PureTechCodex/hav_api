-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 15, 2021 at 11:42 AM
-- Server version: 5.7.36
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devlarav_hav_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `fileDetailsId` int(10) UNSIGNED NOT NULL,
  `folderDetailsId` int(10) UNSIGNED NOT NULL,
  `fileName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileExt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileSavedName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileLocation` int(10) UNSIGNED DEFAULT NULL,
  `isActive` tinyint(1) UNSIGNED DEFAULT NULL,
  `createdBy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modifiedBy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`fileDetailsId`, `folderDetailsId`, `fileName`, `fileExt`, `fileSavedName`, `fileLocation`, `isActive`, `createdBy`, `modifiedBy`, `created_at`, `updated_at`) VALUES
(3, 1, 'kochi_trail.pdf', 'pdf', 'kochi_trail_1597754271.pdf', 1, 1, 'Aakash', 'Aakash', '2020-08-18 07:07:51', '2020-08-18 07:07:51');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `folderDetailsId` int(10) UNSIGNED NOT NULL,
  `asscRecordId` int(11) NOT NULL,
  `folderName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folderParentId` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `createdBy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`folderDetailsId`, `asscRecordId`, `folderName`, `folderParentId`, `isActive`, `createdBy`, `modifiedBy`, `created_at`, `updated_at`) VALUES
(1, 4, 'B', 0, 1, 'Aakash', 'Aakash', '2020-08-18 05:05:48', '2020-08-18 08:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_08_11_035831_create_otps_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('02eb8bc2862d10d4b1977f25a71d773340f3e95270516701894db0197b3f807ed0e06d299f14abc9', 14, 1, 'MyApp', '[]', 0, '2020-08-19 00:01:18', '2020-08-19 00:01:18', '2021-08-19 05:01:18'),
('08cfe38315f41817eb2a7d298dfb41eb8bf9d25bcaa2c2b874a48f54918e7eba8b152daddb830fc9', 17, 1, 'MyApp', '[]', 0, '2020-08-19 02:13:45', '2020-08-19 02:13:45', '2021-08-19 07:13:45'),
('0b7c79367c23af60caed36776c5db3197a23516b0f99606e7c05a0ca61e9a7e3af1ceed86a226bbb', 4, 1, 'MyApp', '[]', 0, '2020-08-15 11:09:56', '2020-08-15 11:09:56', '2021-08-15 16:09:56'),
('129bd8182afcd57f97b068665382e1a7b750863cc52d919973aa7763a1b6e1ff41dcad1d557b1c88', 3, 1, 'MyApp', '[]', 0, '2020-08-10 05:09:11', '2020-08-10 05:09:11', '2021-08-10 10:39:11'),
('1745318cb3b27764a8762d53d7fadf3d3b57df3d2c44c93bb699eef6e5a48902a05f8393cdace863', 4, 1, 'MyApp', '[]', 0, '2020-08-10 05:25:05', '2020-08-10 05:25:05', '2021-08-10 10:55:05'),
('202357b4df53df4005426de6c629f17bab28771adbee3f206bc8b481fdb573ad543f96886f8e54d2', 1, 1, 'MyApp', '[]', 0, '2020-08-10 05:05:12', '2020-08-10 05:05:12', '2021-08-10 10:35:12'),
('21acc2dcef7ebfaa64dcff84a78f7f195b6c2709930fde4510748ba76c4d29bb65b3241ac9c732d0', 9, 1, 'MyApp', '[]', 0, '2020-08-16 02:26:48', '2020-08-16 02:26:48', '2021-08-16 07:26:48'),
('25a0e11f76c6c86df43e961c6ffd5750a73e66e1df8311b86a46175b42d1470da91787d22d371f49', 15, 1, 'MyApp', '[]', 0, '2020-08-19 00:06:05', '2020-08-19 00:06:05', '2021-08-19 05:06:05'),
('319ab3b010b655202bb8d481d2e5df09856b14cbdcc1c1e54b17c1957a06bc2e0230d13bf69a3952', 8, 1, 'MyApp', '[]', 0, '2020-08-16 01:48:58', '2020-08-16 01:48:58', '2021-08-16 06:48:58'),
('38982c23130d6b4424dccc400d70018393979ea41cfe8d651bf70661ecc20066b51b706ce056b7f4', 5, 1, 'MyApp', '[]', 0, '2020-08-13 04:33:54', '2020-08-13 04:33:54', '2021-08-13 09:33:54'),
('3ae6a8fa46c0c8e2af58dc33eb0c22cef3d99a192c2b41c2d1f9e0098faff85d86fe12436b5477bf', 11, 1, 'MyApp', '[]', 0, '2020-08-16 02:58:36', '2020-08-16 02:58:36', '2021-08-16 07:58:36'),
('4e3ef80f1165b8af71dc0002f7e4f7eee6162128c273996b19fb94287262102684ccf1bcf864d4b5', 14, 1, 'MyApp', '[]', 0, '2020-08-18 23:57:07', '2020-08-18 23:57:07', '2021-08-19 04:57:07'),
('66ae321c3e231fcb93583cb618b1fde5965711835f151cb0323cae68bb8c801d8c129d6674114fd5', 2, 1, 'MyApp', '[]', 0, '2020-08-10 05:06:34', '2020-08-10 05:06:34', '2021-08-10 10:36:34'),
('681af56049375a072c57d9792e793b8c2cea31d5041c96f73a70436afa34682b157b42d26b932215', 18, 1, 'MyApp', '[]', 0, '2020-08-20 00:45:33', '2020-08-20 00:45:33', '2021-08-20 05:45:33'),
('721f9a57b50e4a31e772489baeecc108f0e35db0552ba3ecd6e2c8438cf54c4187ba7f4fb0627dcb', 10, 1, 'MyApp', '[]', 0, '2020-08-16 02:56:24', '2020-08-16 02:56:24', '2021-08-16 07:56:24'),
('7cf456de13038f20938a67c196cb3ba587cc0ab1c75bee71ccc6c66e77d1d5ee70aa35ed2faaed5a', 7, 1, 'MyApp', '[]', 0, '2020-08-16 00:15:38', '2020-08-16 00:15:38', '2021-08-16 05:15:38'),
('917600cf8be5f44ec5d8f2be29f86b5fdf4d960db1b58524b451946843a50bfb8b45767149ca2ced', 6, 1, 'MyApp', '[]', 0, '2020-08-13 06:00:06', '2020-08-13 06:00:06', '2021-08-13 11:00:06'),
('9a75f714ac860161a8db2cb72d0fe7c8b9492ce43097b011fab5de4b3e16213d8435b1185dc643ff', 13, 1, 'MyApp', '[]', 0, '2020-08-16 03:43:12', '2020-08-16 03:43:12', '2021-08-16 08:43:12'),
('b476b5338d1286e6fdc50677322cc5ee5e0f077ac3dba2ba135e7f9764a8d01b7a6dba546d5b308f', 16, 1, 'MyApp', '[]', 0, '2020-08-19 00:09:35', '2020-08-19 00:09:35', '2021-08-19 05:09:35'),
('d2d5ac36c1d1583bd0af2ebbeb5743d54c7f13e6fe4282c58982f20fc827bccdb9eaeb7d2d18a9d9', 14, 1, 'MyApp', '[]', 0, '2020-08-19 00:01:53', '2020-08-19 00:01:53', '2021-08-19 05:01:53'),
('df155c4c649102adb11cab1f7d180da74eedf73fcd227e85895570b66d1d11db93a4a5c8ba2c2527', 12, 1, 'MyApp', '[]', 0, '2020-08-16 03:07:22', '2020-08-16 03:07:22', '2021-08-16 08:07:22'),
('e914544c4021c7f0ebda7fd0dd0739c4db0db1740ad46c92094f5a07e5a39bdc5422859d922b25fe', 18, 1, 'MyApp', '[]', 0, '2020-08-19 03:46:59', '2020-08-19 03:46:59', '2021-08-19 08:46:59'),
('f1b9278517d8abb54e307dc4bfb1c29dc7c25f451cbfc9c8dded17ddb1b06aba315f79483d5e95dc', 4, 1, 'MyApp', '[]', 0, '2020-08-10 05:10:52', '2020-08-10 05:10:52', '2021-08-10 10:40:52'),
('f4a4472ae8cff06ca695c737d9f13d5203ef3287944fcc88ad3e1c4d82215cbe6785d5a758693c56', 6, 1, 'MyApp', '[]', 0, '2020-08-13 05:57:19', '2020-08-13 05:57:19', '2021-08-13 10:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'sj7FpEORoe4MPUBGCUMgdgmOI1A5iW51XTcCAQ7h', NULL, 'http://localhost', 1, 0, 0, '2020-08-10 04:36:14', '2020-08-10 04:36:14'),
(2, NULL, 'Laravel Password Grant Client', '1Urg6NEbbYNTlGTwHIIyjclDkL5SaNqmcfGTNacq', 'users', 'http://localhost', 0, 1, 0, '2020-08-10 04:36:14', '2020-08-10 04:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-08-10 04:36:14', '2020-08-10 04:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verify_string` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otps`
--

INSERT INTO `otps` (`id`, `phone`, `otp`, `verify_string`, `created_at`, `updated_at`) VALUES
(1, '8830705470', '3079', 'asf2d15safasf', '2020-08-13 05:22:19', '2020-08-13 05:22:19'),
(2, '8830705470', '3919', 'asf2d15safasf', '2020-08-13 06:01:55', '2020-08-13 06:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_string` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_refral` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `mname`, `lname`, `phone`, `email`, `email_verified_at`, `password`, `remember_token`, `verification_string`, `user_refral`, `status`, `created_at`, `updated_at`) VALUES
(18, 'Prakash', 'n', 'kothule', '8030705471', '2904aakash.kothule@gmail.com', NULL, '$2y$10$G1cg8Ux6gm2GMxeokQutOee7NyT69Bp0fRHeyfBZn0oRbOoHB9ZUi', NULL, '5f3ce6ffe221c', '', 'Active', '2020-08-19 03:46:58', '2020-08-20 00:58:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`fileDetailsId`),
  ADD KEY `files_folderdetailsid_index` (`folderDetailsId`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`folderDetailsId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `fileDetailsId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `folderDetailsId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `otps`
--
ALTER TABLE `otps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
